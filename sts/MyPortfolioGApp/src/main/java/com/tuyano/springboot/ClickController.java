package com.tuyano.springboot;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ClickController {

	@RequestMapping(value = "/index.html", method = RequestMethod.GET)
	public ModelAndView jindex(ModelAndView mav) {
		mav.setViewName("index");
		mav.addObject("msg","MyDataのサンプルです。");
		return mav;
	}
	
	@RequestMapping(value = "/johnnys.html", method = RequestMethod.GET)
	public ModelAndView johnnys(ModelAndView mav) {
		mav.setViewName("johnnys");
		mav.addObject("msg","MyDataのサンプルです。");
		return mav;
	}
	
	@RequestMapping(value = "/butai.html", method = RequestMethod.GET)
	public ModelAndView butai(ModelAndView mav) {
		mav.setViewName("butai");
		mav.addObject("msg","MyDataのサンプルです。");
		return mav;
	}
	
	@RequestMapping(value = "/syumi.html", method = RequestMethod.GET)
	public ModelAndView syumi(ModelAndView mav) {
		mav.setViewName("syumi");
		mav.addObject("msg","MyDataのサンプルです。");
		return mav;
	}
	
	@RequestMapping(value = "/toiawase.html", method = RequestMethod.GET)
	public ModelAndView toiawase(ModelAndView mav) {
		mav.setViewName("toiawase");
		mav.addObject("msg","MyDataのサンプルです。");
		return mav;
	}
}
