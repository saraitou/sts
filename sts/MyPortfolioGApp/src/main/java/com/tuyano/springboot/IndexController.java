package com.tuyano.springboot;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestParam;

import com.tuyano.springboot.repositories.MyDataMongoRepository;

@Controller
public class IndexController {

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView index(ModelAndView mav) {
		mav.setViewName("index");
		mav.addObject("msg", "MyDataのサンプルです。");
		return mav;
	}

	@RequestMapping(value = "/johnnys", method = RequestMethod.GET)
	public ModelAndView johnnys(ModelAndView mav) {
		mav.setViewName("johnnys");
		mav.addObject("msg", "MyDataのサンプルです。");
		return mav;
	}

	@RequestMapping(value = "/butai", method = RequestMethod.GET)
	public ModelAndView butai(ModelAndView mav) {
		mav.setViewName("butai");
		mav.addObject("msg", "MyDataのサンプルです。");
		return mav;
	}

	@RequestMapping(value = "/syumi", method = RequestMethod.GET)
	public ModelAndView syumi(ModelAndView mav) {
		mav.setViewName("syumi");
		mav.addObject("msg", "MyDataのサンプルです。");
		return mav;
	}

	@RequestMapping(value = "/toiawase", method = RequestMethod.GET)
	public ModelAndView toiawase(ModelAndView mav) {
		mav.setViewName("toiawase");
		mav.addObject("msg", "MyDataのサンプルです。");
		return mav;
	}
	
	@RequestMapping(value = "/toiawase", method = RequestMethod.POST)
	public ModelAndView toiawase(HttpServletRequest request,
			ModelAndView mav) {
		String param = request.getParameter("email");
		mav.setViewName("toiawase");
		mav.addObject("msg", "MyDataのサンプルです。");
		return mav;
	}
}